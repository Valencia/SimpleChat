////////////////////////////////////////////////////////////////////////////////
// General
////////////////////////////////////////////////////////////////////////////////
var gulp    = require('gulp');
var changed = require('gulp-changed');
var tsc     = require('gulp-typescript');
var uglify  = require('gulp-uglify');
var jade    = require('gulp-jade');

// Configuración para Jade
var jade_cfg = {
    pretty: true
}

// Tarea general de construcción.
gulp.task('default', ['make-server', 'make-client']);

////////////////////////////////////////////////////////////////////////////////
// Servidor
////////////////////////////////////////////////////////////////////////////////

// Configuración del compilador para el servidor.
var server_tsc_cfg = {
    module: 'commonjs',     // el módulo usado por NodeJS es CommonJS
    target: 'ES3',          // el estandar ECMA a usar, en este caso ES3
    removeComments: true    // los comentarios no son necesarios
}

// Tarea para construir los scripts del servidor
gulp.task('make-server-scripts', function() {
    gulp.src('src/server/**/*.ts')
        .pipe(changed('src/server/**/*.ts'))
        .pipe(tsc(server_tsc_cfg))
        .pipe(gulp.dest('app'));
});

// Tarea para construir el servidor
gulp.task('make-server', ['make-server-scripts']);

////////////////////////////////////////////////////////////////////////////////
// Cliente
////////////////////////////////////////////////////////////////////////////////

// Configuración del compilador para el cliente
var client_tsc_cfg = {
    module: 'amd',          // RequireJS trabaja sobre el módulo AMD
    target: 'ES3',          // el estandar ECMA a usar, en este caso ES3
    removeComments: true    // los comentarios no son necesarios
}


// Tarea para construir cliente
gulp.task('make-client', [
    'make-client-templates',
    'make-client-styles',
    'make-client-scripts',
    'client-requirejs',
    'client-jquery',
    'client-bootstrap',
    'client-angularjs'
]);

////////////////////////////////////////////////////////////////////////////////
// Tarea para la construcción de las plantillas Jade
gulp.task('make-client-templates', function() {
    gulp.src('src/client/templates/*.jade')
        .pipe(changed('src/client/templates/**/*.jade'))
        .pipe(jade(jade_cfg))
        .pipe(gulp.dest('app/client'));
});

////////////////////////////////////////////////////////////////////////////////
// Tarea para la construcción de los estilos css
gulp.task('make-client-styles', function() {
    gulp.src('src/client/styles/*.css')
        .pipe(changed('src/client/styles/*.css'))
        .pipe(gulp.dest('app/client/styles'));
});

////////////////////////////////////////////////////////////////////////////////
// Tarea para la construcción de los scripts
gulp.task('make-client-scripts', function() {
    gulp.src('src/client/**/*.ts')
        .pipe(changed('src/client/**/*.ts'))
        .pipe(tsc(client_tsc_cfg))
        .pipe(gulp.dest('app/client/scripts'));

    gulp.src('src/client/**/*.js')
        .pipe(gulp.dest('app/client/scripts'));
});

////////////////////////////////////////////////////////////////////////////////
// Tarea para la inclusión de la dependencia
// requirejs
gulp.task('client-requirejs', function() {
    var requirejs_dir = "node_modules/requirejs";

    gulp.src(requirejs_dir + "/require.js")
        .pipe(gulp.dest('app/client/scripts/lib'));
});

////////////////////////////////////////////////////////////////////////////////
// Tarea para la inclusión de la dependencia
// jquery
gulp.task('client-jquery', function() {
    var jquery_dir = 'node_modules/jquery/dist';

    gulp.src(jquery_dir + '/jquery.js')
        .pipe(gulp.dest('app/client/scripts/lib'));
});

////////////////////////////////////////////////////////////////////////////////
// Tarea para la inclusión de la dependencia
// bootstrap
gulp.task('client-bootstrap', function() {

    var bootstrap_dir = 'node_modules/bootstrap/dist';

    gulp.src(bootstrap_dir + '/css/bootstrap.css')
        .pipe(gulp.dest('app/client/styles'));

    gulp.src(bootstrap_dir + '/js/bootstrap.js')
        .pipe(gulp.dest('app/client/scripts/lib'));
});


////////////////////////////////////////////////////////////////////////////////
// Tarea para la inclusión de la dependencia
// angularjs
gulp.task('client-angularjs', function() {

    var angularjs_dir = 'node_modules/angular';

    gulp.src(angularjs_dir + '/angular.js')
        .pipe(gulp.dest('app/client/scripts/lib'));
});
