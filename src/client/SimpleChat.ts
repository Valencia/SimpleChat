/*
 * This file is part of SimpleChat.
 *
 * SimpleChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SimpleChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SimpleChat.  If not, see <http://www.gnu.org/licenses/>.
 */

///<reference path="../external/angularjs/angular.d.ts" />

import AngularJS = require('angular');
import Client = require('./Client');

/** Clase principal de la aplicación.
 *
 *  Esta clase representa el punto de entrada principal
 *  de la aplicación, exponiendo un método general de
 *  inicialización.
 */
class SimpleChat
{
    /** Punto de entrada principal.
     */
    public static main(): void
    {
        var app: AngularJS.IModule; //!< aplicación AngularJS

        // registro el módulo principal y registro el controlador
        // para el cliente.
        app = AngularJS.module('SimpleChat', []);
        app.controller('Client', ['$scope', (scope) => new Client(scope)]);

        // lanzo la aplicación AngularJS
        AngularJS.bootstrap(document, ['SimpleChat']);
    }

}; // class SimpleChat

export = SimpleChat;
