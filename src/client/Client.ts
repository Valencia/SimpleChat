/*
 * This file is part of SimpleChat.
 *
 * SimpleChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SimpleChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SimpleChat.  If not, see <http://www.gnu.org/licenses/>.
 */

///<reference path="../external/angularjs/angular.d.ts" />

import AngularJS = require('angular');
import ChatClient = require('./ChatClient');
import IClient = require('./IClient');
import IMessage = require('./IMessage');

/** Clase base para la representación y manipulación
 *  de la interfaz gráfica de usuario.
 */
class Client implements IClient
{
    public message: string;         //!< El mensaje de la interfaz de usuario
    private m_scope: IClientScope;  //!< El ámbito del entorno AngularJS
    private m_chat: ChatClient;     //!< El cliente de chat

    /** Crea una nueva instancia de un cliente.
     *
     *  @param[in] scope
     *      El ámbito AngularJS de la aplicación.
     */
    public constructor(scope)
    {
        this.m_scope = scope;
        this.m_chat = new ChatClient(this);
        this.message = "";

        // inicializo el cliente de chat
        this.m_chat.start();

        // agrego la variable al ámbito AngularJS para
        // poder realizar una comunicación directa con
        // la interfaz gráfica de usuario
        this.m_scope.client = this;
    }

    /** Obtiene una cadena con la cantidad de usuarios
     *  en linea.
     *
     *  @return
     *      Un mensaje sencillo indicando la cantidad
     *      de usuarios conectados.
     */
    public getUsersConnected(): string
    {
        var count: number;

        count = this.m_chat.getUsersConnected();

        if (count > 1)
        {
            return "Hay " + count + " usuarios en línea";
        }
        else
        {
            return "Sos el único usuario en línea";
        }
    }

    /** Obtiene la lista de mensajes (historial) del chat.
     *
     *  @return
     *      Un array conteniendo las lineas del historial
     *      de chat.
     */
    public getMessagesList(): IMessage[]
    {
        return this.m_chat.getHistory();
    }

    /** Realiza la actualización de la interfaz gráfica de
     *  usuario.
     */
    public refresh(): void
    {
        var chat_history: HTMLElement;  // para acceder al historial

        // actualizo el "scope", es decir lo relacionado al
        // ámbito de AngularJS
        this.m_scope.$apply();

        // el objecto "document" es fijo en JavaScript y por lo
        // tanto en TypeScript; se utiliza para obtener acceso
        // al documento general (página web) por lo que extraigo
        // el "div" relacionado con el historial de chat y
        // actualizo su posición de "scroll"
        chat_history = document.getElementById('chat-history');
        chat_history.scrollTop = chat_history.scrollHeight;
    }

    /** Método de llamada para enviar un mensaje al
     *  servidor.
     *
     *  Este método realiza el envío del mensaje indicado
     *  por el usuario hacia el servidor.
     */
    protected sendMessage(): void
    {
        if (this.message != "")
        {
            this.m_chat.sendMessage(this.message);
            this.message = "";
        }
    }

}; // class Client

/** Interfaz privada para modelizar el ámbito del controlador
 *  cliente.
 */
interface IClientScope extends AngularJS.IScope
{
    client: Client; //!< Instancia del controlador.

}; // interface IClientScope

// Exporto explícitamente la clase pública.
export = Client;
